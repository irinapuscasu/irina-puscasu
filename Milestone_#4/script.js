var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");
var clicked = false;

var obj = {};
var timer;
function SaveText(text, filename) {
    var a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click()
}

function ReadFromJson(filename) {
    d3.json(filename, function (data) {
        this.obj = data;

        function removeLine(d) {
            d3.event.stopPropagation();
            data.links = data.links.filter(function (e) { return e != d; });
            d3.select(this)
                .remove();
        }

        function removeElement(d) {
            d3.event.stopPropagation();
            data.nodes = data.nodes.filter(function (e) { return e != d; });
            d3.select(this)
                .remove();
            for (var i = 0; i < data.links.length; i++) {
                if (d.id == data.links[i].source || d.id == data.links[i].target) {
                    removeLine(data.links[i]);
                    i = i - 1;
                }
            }


            d3.selectAll("svg > *").remove();

            svg.selectAll("line")
                .data(data.links)
                .enter()
                .append("line")
                .attr("x1", function (d) {
                    for (var i = 0; i < data.nodes.length; ++i) {
                        if (data.nodes[i].id === d.source)
                            return data.nodes[i].x
                    }
                })
                .attr("y1", function (d) {
                    for (var i = 0; i < data.nodes.length; ++i) {
                        if (data.nodes[i].id === d.source)
                            return data.nodes[i].y
                    }
                })
                .attr("x2", function (d) {
                    for (var i = 0; i < data.nodes.length; ++i) {
                        if (data.nodes[i].id === d.target)
                            return data.nodes[i].x
                    }
                })
                .attr("y2", function (d) {
                    for (var i = 0; i < data.nodes.length; ++i) {
                        if (data.nodes[i].id === d.target)
                            return data.nodes[i].y
                    }
                })
                .attr("stroke-width", 4)
                .style("stroke", "black")
                .on("click", removeLine);

            svg.selectAll("circle")
                .data(data.nodes)
                .enter()
                .append("circle")
                .attr("id", function (d) { return d.id; })
                .attr("cx", function (d) { return d.x; })
                .attr("cy", function (d) { return d.y; })
                .style("fill", "black")
                .attr("r", "10px")
                .on("click", removeElement);

            var text = svg.selectAll("text")
                .data(data.nodes)
                .enter()
                .append("text");

            text.attr("x", function (d) { return d.x - 5; })
                .attr("y", function (d) { return d.y + 5; })
                .text(function (d) { return d.id; })
                .attr("font-family", "sans-serif")
                .attr("font-size", "15px")
                .attr("fill", "red");

        }


        svg.selectAll("line")
            .data(data.links)
            .enter()
            .append("line")
            .attr("x1", function (d) { return data.nodes[d.source].x })
            .attr("y1", function (d) { return data.nodes[d.source].y })
            .attr("x2", function (d) { return data.nodes[d.target].x })
            .attr("y2", function (d) { return data.nodes[d.target].y })
            .attr("stroke-width", 4)
            .style("stroke", "black")
            .on("click", removeLine);

        svg.selectAll("circle")
            .data(data.nodes)
            .enter()
            .append("circle")
            .attr("id", function (d) { return d.id; })
            .attr("cx", function (d) { return d.x; })
            .attr("cy", function (d) { return d.y; })
            .style("fill", "black")
            .attr("r", "10px")
            .on("click", removeElement);

        var text = svg.selectAll("text")
            .data(data.nodes)
            .enter()
            .append("text");

        text.attr("x", function (d) { return d.x - 5; })
            .attr("y", function (d) { return d.y + 5; })
            .text(function (d) { return d.id; })
            .attr("font-family", "sans-serif")
            .attr("font-size", "15px")
            .attr("fill", "red");




        var id = data.nodes.length;
        svg.on("click", function () {

            var newData = {
                id: id,
                x: d3.event.x,
                y: d3.event.y
            };

            data.nodes.push(newData);
            id++;


            svg.selectAll("circle")
                .data(data.nodes)
                .enter()
                .append("circle")
                .attr("id", function (d) { return d.id; })
                .attr("cx", function (d) { return d.x; })
                .attr("cy", function (d) { return d.y; })
                .style("fill", "black")
                .attr("r", "10px")
                .on("click", removeElement);

            var text = svg.selectAll("text")
                .data(data.nodes)
                .enter()
                .append("text");

            text.attr("x", function (d) { return d.x - 5; })
                .attr("y", function (d) { return d.y + 5; })
                .text(function (d) { return d.id; })
                .attr("font-family", "sans-serif")
                .attr("font-size", "15px")
                .attr("fill", "red");




            svg.on("mousedown", drawLine);

            function drawLine() {


                var point = d3.mouse(this),
                    node = { x: point[0], y: point[1] };

                var div = d3.select(this)
                    .classed("active", true);

                var w = d3.select(window)
                    .on("mouseup", mouseup);

                function mouseup() {
                    var newLine = {
                        x1: node.x,
                        y1: node.y,
                        x: d3.event.x,
                        y: d3.event.y
                    };

                    for (var i = 0; i < data.nodes.length; i++) {
                        if (newLine.x1 - data.nodes[i].x <= 10 && newLine.x1 - data.nodes[i].x >= 0 && newLine.y1 - data.nodes[i].y >= 0 && newLine.y1 - data.nodes[i].y <= 10) {
                            var s = data.nodes[i].id;
                        }
                        else if (newLine.x1 - data.nodes[i].x >= -10 && newLine.x1 - data.nodes[i].x <= 0 && newLine.y1 - data.nodes[i].y <= 0 && newLine.y1 - data.nodes[i].y >= -10) {
                            var s = data.nodes[i].id;
                        }
                        if (newLine.x - data.nodes[i].x <= 10 && newLine.x - data.nodes[i].x >= 0 && newLine.y - data.nodes[i].y >= 0 && newLine.y - data.nodes[i].y <= 10) {
                            var t = data.nodes[i].id;
                        }
                        else if (newLine.x - data.nodes[i].x >= -10 && newLine.x - data.nodes[i].x <= 0 && newLine.x - data.nodes[i].x <= 0 && newLine.y - data.nodes[i].y >= -10) {
                            var t = data.nodes[i].id;

                        }

                    }
                    var Link = {
                        source: s,
                        target: t
                    };
                    if (newLine.x - node.x != 8 && newLine.y - node.y != 8) {
                        data.links.push(Link);
                    }



                    svg.selectAll("line")
                        .data(data.links)
                        .enter()
                        .append("line")
                        .attr("x1", node.x)
                        .attr("y1", node.y)
                        .attr("x2", newLine.x)
                        .attr("y2", newLine.y)
                        .attr("stroke-width", 4)
                        .attr("stroke", "black")
                        .on("click", removeLine);

                    div.classed("active", false);
                    w.on("mouseup", null);
                }




            };


        });

    })

};

function print() {
    clicked = true;
    console.log(obj);
    SaveText(JSON.stringify(obj), "newGraph.json");
}