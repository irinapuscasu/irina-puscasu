# Task 1

Aplicatie display grafuri.

## Descriere

Se va crea o aplicatie care poate sa incarce niste grafuri din fisier, sa le afiseze intr-o pagina HTML (mai precis intr-o aplicatie desktop Electron).

Aplicatia va fi hostata pe contul tau de:

- code.siemens.com (daca ai acces).
- github.com (daca nu ai acces pe code.siemens.com)

Vei alege un library de `js` pentru desenare de grafuri.

**Recomandare** alege una din:

- D3 (<https://d3js.org/)>
- Chart.js (<http://www.chartjs.org/>)
- HighCharts (<https://www.highcharts.com/)> (doar varianta non-commercial)
- *e ok si alta propusa de tine*.

Pentru fiecare milestone, vei avea nevoie si de un fisier README.md folosind sintaxa `Markdown`. Acest fisier va contine pasii pe care ii faci pentru a ajunge la rezultat.
Fiecare milestone va ajunge intr-un subproiect separat.

## Milestones

**Optional**: poate nu strica si un *milestone 2.5*, care sa probeze integrarea intre Electron si framework-ul folosit. Exista si proiecte care arata cum functioneaza integrarile. De ex:

- <https://www.npmjs.com/package/electron-chartjs>
- <https://www.npmjs.com/package/desktop-3d-app-boilerplate>

### Milestone 1

- Creaza o aplicatie Electron (T01_M1).
- Incarca un fisier json in aplicatie si afiseaza continutul in consola (`console.log`)
- Incarca un fisier json in aplicatie si afiseaza continutul in pagina HTML (de ex: intr-un text edit)

Nota: se poate incarca fie folosind ``fs`` din node.js (varianta usoara si recomandata: <https://www.w3schools.com/nodejs/nodejs_filesystem.asp>), fie pentru masochisti cu extensie de C++.

### Milestone 2

- Creaza o aplicatie Electron (T01_M2).
- Afiseaza in pagina HTML:
  - 1 buton de browse, care sa iti dea voie sa selectezi fisierul de incarcat (scop: doar fisiere json).
  - 1 text edit care sa afiseze ce fisier ai selectat cu browse.
  - 1 buton de load care sa incarce fisierul selectat.
  - 1 zona de afisare a continutului fisierului selectat (ex: un text edit)
- Elementele din pagina trebuie sa raspunda la comenzi.

### Milestone 3

- Creaza o aplicatie Electron (T01_M3).
- Afiseaza in pagina HTML:
  - 1 buton de browse, care sa iti dea voie sa selectezi fisierul de incarcat (scop: doar fisiere json).
  - 1 text edit care sa afiseze ce fisier ai selectat cu browse.
  - 1 buton de load care sa incarce fisierul selectat.
  - 1 grafic (canvas sau ce pune library-ul la dispozitie) de afisare a continutului fisierului selectat
- Defineste un format de definit pentru fisierul json (dupa cum vrei tu). Acesta va contine nodurile din graf.
- Deseneaza pe ecran (folosind un library) nodurile.

### Milestone 4

- Creaza o aplicatie Electron (T01_M4).
- Trebuie sa faca tot ce face T01_M3 si in plus:
  - incarca si afiseaza si muchiile, pe langa noduri.
